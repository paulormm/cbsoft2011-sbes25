require 'fileutils'

whitelist = File.readlines('maintrack.whitelist').map(&:strip)

Dir.glob('maintrack/*/*.pdf').each do |file|
  if !whitelist.include?(file)
    puts "mv #{file} #{file}.exclude"
    FileUtils.mv(file, file + '.exclude')
  end
end
