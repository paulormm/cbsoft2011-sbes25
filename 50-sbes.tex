\section{FLOSS in SBES}
\label{sec:floss-sbes}

To evaluate where the Brazilian Software Engineering research
community stands with regard to FLOSS, we carried out a study on the
papers published in the Brazilian Symposium on Software Engineering
(SBES). We have examined:
%
(i) main track papers between 1999 and 2010,
(ii) tools session papers between 2001 and 2010, and
(iii) Software Engineering Education Forum (\emph{Fórum de Education em
Engenharia de Software} -- FEES) papers from 2008, its first edition, to 2010.
%
This section describes this study and its findings.

\subsection{Data acquisition}

We obtained the full text of SBES main track, tools session, and FEES
papers from the following sources:

\begin{itemize}
  \item
    Main track papers from SBES 1999, 2001, 2002, 2004, 2006, 2007, 2008, and
    2009 were obtained from the Brazilian Computer Science Digital
    Library\footnote{\url{http://www.lbd.dcc.ufmg.br/bdbcomp}} (``Biblioteca
    Digital Brasileira de Computação'' -- BDBComp), maintained by UFMG's
    Databases Laboratory.
    %
  \item
    Main track papers from SBES 2005 were obtained contacting the program chair
    and from SBES 2010 were obtained from the IEEE Xplore Digital
    Library\footnote{\url{http://ieeexplore.ieee.org}}.
  \item
    Tools papers from SBES 2001 and 2002 were also obtained from BDBComp.
    %
    Because SBES tools session papers from most years are not available
    on-line, we got 2004, 2005, 2006, 2007, 2008, 2009, and 2010 papers	
    contacting the program chair of each edition.
  \item
    FEES papers from 2008 and 2009 were obtained at the FEES homepage
    hosted by PUC-Rio\footnote{\url{http://fees.inf.puc-rio.br/}}, and
    2010 papers were obtained from CBSoft 2010 organizers.
\end{itemize}

\rowcolors{3}{lightgray}{white}
\input{maintrack-paper-count.ltx}

\rowcolors{3}{lightgray}{white}
\input{tools-paper-count.ltx}

The number of main track and tools papers analyzed by year are presented
in Tables~\ref{table:maintrack:papers-by-year} and
\ref{table:tools:papers-by-year}, respectively. FEES papers were 14
from 2008, 8 for 2009 and 8 for 2010.
%
Despite not being able to obtain every single paper since 1999, the
papers we obtained gave us a reasonable approximation of what happened
during the period. We analyzed a total amount of 378 papers: 206 from
the main track, 142 from the tools session, and 30 from the Software
Engineering Education Forum.

The PDF files were processed by a script that extracted their text,
and matched their contents against the following terms:
``software(s) livre(s)'',
``ferramenta(s) livre(s)'',
``ferramenta(s) aberta(s)'',
``software(s) aberto(s)'',
``código aberto'',
``repositório(s) de software'',
``free software'',
``open source'',
``open software'',
``libre software'',
``software repository'',
``OSS'',
``FLOSS'',
``FOSS'', and
``OSSD''.
To validate this identification process of the papers related to FLOSS,
we checked all of them manually by reading the abstract and the sections that
include one of the terms above. 

Tables \ref{table:maintrack:papers-by-year} and
\ref{table:tools:papers-by-year} present the raw results for the SBES
main track and tools session, respectively.  The tables presents the
number of papers analyzed in each year, how many of them mentioned
FLOSS, and the relative frequency of papers mentioning FLOSS (i.e. the
ratio between the number of papers mentioning FLOSS and the total of
papers).
%
We can see that the number of papers mentioning FLOSS in both the main
track and the tools session have been increasing, getting to around 50\%
in their 2010 editions.

\def\sourceFN{\footnote{\url{https://gitorious.org/flosspapers/cbsoft2011-sbes25}}}

The source code for the data extraction and analysis scripts we used is
available together with the sources for this paper itself\sourceFN, and
is licensed under the GNU General Public License version 3 or any later
version.

%------------------------------------------------------------------------------

\subsection{Analysis of research papers mentioning FLOSS}

\begin{table*}[ht]
\begin{center}
\caption{Analysis of research papers mentioning FLOSS}
\label{table:maintrack-papers}
\begin{tabular}{lrrrrrrr|r}
  \hline
    Category                & 2002 & 2004 & 2005 & 2007  & 2008  & 2009  & 2010  & Total \\
  \hline
    Reference               & 0    & 0    & 0    & 1     & 0     & 1     & 1     & 3 \\
    Example or Comparison   & 1    & 0    & 0    & 0     & 0     & 1     & 1     & 3 \\
    Running FLOSS tools     & 1    & 1    & 2    & 0     & 1     & 1     & 0     & 6 \\
    FLOSS tool as target    & 0    & 0    & 0    & 1     & 0     & 0     & 3     & 4 \\
    Data from FLOSS         & 0    & 0    & 0    & 0     & 0     & 2     & 3     & 5 \\
    Research about FLOSS    & 0    & 0    & 0    & 0     & 0     & 1     & 3     & 4 \\
  \hline
    Total                   & 2    & 1    & 2    & 2     & 1     & 6     & 11    & 25 \\
  \hline
\end{tabular}
\vspace{-1em}
\end{center}
\end{table*}

We investigated in which way the FLOSS terms were used in these 25
papers. For that, we classified them in 6 different categories, according
to our interpretation of each paper.
%
Each paper was classified in exactly one category, as shown in
Table~\ref{table:maintrack-papers}.
%
The first category comprised three papers that did not mention FLOSS
explicitly in their text but just referenced bibliography whose title
mentioned FLOSS~\cite{oliveira2007, prudencio2009:sbes, silva2010:sbes}.
%
The other categories are detailed below.

\subsubsection{Example or Comparison}
Papers that are not focused on FLOSS but used FLOSS as an example or comparison.
%
% Uma Arquitetura Reflexiva Baseada na Web para Ambiente de Suporte a Processo
\textbf{Yamaguti and Price}~\cite{yamaguti2002} presented a web-based reflective architecture.
They argued that any FLOSS CASE tool is an example of ``open object'', a concept defined
in their paper.
%
% An Integrated Approach for Model Driven Process Modeling and Enactment
\textbf{Maciel \textit{et al.}}~\cite{maciel2009:sbes} explained that there are
several FLOSS and proprietary MDD/MDA tools with different features that can use
their proposed approach for model-driven process.
% SDiff: A comparison tool based in syntactical document structure
\textbf{Ara\'ujo and von Staa}~\cite{araujo2010:sbes} describe a tool, called SDiff, that
compares documents using their syntactic structure. They compared SDiff to
KDiff, which is FLOSS tools also used to analyse documents.
%
\subsubsection{Running FLOSS tools}
Some of the analyzed papers described the development or use of FLOSS tools to carry
out their research.
%
% RDF na Interoperabilidade entre Domínios na Web
\textbf{Santos and Schiel}~\cite{santos2002} presented a study about the interoperability of data
between different domains on the Internet, applying the Resource Description
Framework (RDF). They used PostgreSQL (a FLOSS database) in one of their case studies.
%Building Flexible Refactoring Tools with XML
\textbf{Mendonça \textit{et al.}}~\cite{mendonca2004:sbes} among their contributions
presented a refactoring framework, called RefaX. They used Jikes, which is an
adaptation from IBM’s FLOSS Java compiler to implement a Java version of Refax,
caled Refax4Java.
%Uma Linguagem de Workflow Para Composicao de Web Services - LCWS
\textbf{Maciel and Yano}~\cite{maciel2005:sbes} proposed a workflow language based Web Services and
a FLOSS run-time environment to this language.
%A Component-based Product Development Process for a Workflow Management System Product Line
\textbf{Gimenes \textit{et al.}}~\cite{gimenes2005:sbes} presented a process for component-based
product line for Workflow Management Systems. The implementation of a proposed
product line was basead on FLOSS frameworks and tools such as JHotDraw, JacORB,
CORORB, MySQL, and ObjectBridge.
%
% Obtaining Trustworthy Test Results in Multi-threaded Systems
\textbf{Dantas \textit{et al.}}~\cite{dantas2008} proposed an approach to test multi-threaded
systems, using  OurGrid, a FLOSS peer-to-peer grid middleware.
Also, they developed a FLOSS testing framework called ThreadControl.
%
% Towards a UML profile for model-driven object-relational mapping
\textbf{Torres \textit{et al.}}~\cite{torres2009:sbes} proposed the Model
Driven Java Persistence API (MD-JPA). A FLOSS plug-in was developed
to validate and work with MD-JPA.
%
\subsubsection{FLOSS tool as target}
Several of the selected papers used FLOSS tools as a target to test another tool, framework,
or environment.
%
% Um Ambiente para Detecção de Cenários Implícitos a partir de Rastros de Execução
\textbf{Souza and Mendonça}~\cite{souza2007} defined a reverse engineering environment to
extract and detect ``implied scenarios'' from traces. They tested the
proposed environment against MyPetStore, a customized version of PetStore, which is
a FLOSS tool developed by Sun MicroSystems to illustrate programming resources available in
the J2EE platform.
%
% Characterising Faults in Aspect-Oriented Programs: towards Filling the Gap
% between Theory and Practice
\textbf{Ferrari \textit{et al.}}~\cite{ferrari2010:sbes} performed an
empirical study to quantify, document, and classify faults uncovered in
several releases of three Aspect-Oriented systems. One of these systems
was iBATIS, a Java-based FLOSS framework for object-relational data
mapping.
%
% Defining and Applying Detection Strategies for Aspect-Oriented Code Smells
\textbf{Macia \textit{et al.}}~\cite{macia2010:sbes} also used iBATIS as target in a study on
Aspect-Oriented programming. They presented a set of metric-based
strategies to detect recurring code smells in existing AO systems.
%
% Software Reuse versus Stability: Evaluating Advanced Programming Techniques
\textbf{Dantas and Garcia}~\cite{dantas2010:sbes} analyzed two software products,
among them the iBATIS
tool, to perform an analysis of the relationship between advanced programming techniques
and the trade-off of software reuse and stability.
%
\subsubsection{Data from FLOSS}
Part of the papers used data from FLOSS projects to test their tools and theories.
%
% Mining Software Change History in an Industrial Environment
\textbf{Colaço Jr. \textit{et al.}}~\cite{junior2009:sbes} analyzed 18 large Brazilian
industrial projects trying to use association rules obtained from mining
their source code repositories to predict pairs of files that would need to
be changed together in the future. They compared their results with
other results from a previous similar study with eight FLOSS projects (Eclipse, GCC,
Gimp, JBOSS, JEdit, KOffice, Postgres, and Python). They found that, in
that industrial environment, the prediction power was even higher than with
the FLOSS projects.
%
%Reference Values for Object-Oriented Software Metrics
\textbf{Ferreira \textit{et al.}}~\cite{ferreira2009:sbes} collected source code
metrics from 40 Java FLOSS projects to perform an empirical study and propose reference
values for Object-Oriented software metrics.
%
% Identifying Code Smells with Multiple Concern Views
\textbf{Carneiro \textit{et al.}}~\cite{carneiro2010:sbes} conducted an experimental
study to assess a multiple views approach based on concern-driven software
visualization resources, arguing that visual views support code smell detection.
%
To perform their study, they analyzed five consecutive versions of a FLOSS
project called MobileMedia, an application for photo, music, and video on
mobile devices.
%
% Evaluating the Implications of a Package Design Principle upon Software Maintainability
\textbf{Costa and Barros}~\cite{costa2010:sbes} analyzed eigth FLOSS projects (Azureus, Eclipse ANT,
Jena, JMule, JUnit, JVI, Poor Man CMS, and Sweet Home 3D) to perform an
experimental study to observe if the adoption of the Common-Closure
principle improves a set of software design metrics, based on a proposed
technique to organize the classes into packages according to this principle.
%
% An Automated Approach for Scheduling Bug Fix Tasks
\textbf{Netto \textit{et al.}}~\cite{netto2010:sbes} used data from
Eclipse Bugzilla to test a method based on a genetic algorithm to find
the closest optimal bug correction task schedules, according to the
relevance of information from bug repositories. As a practical result,
they suggested a better schedule to Eclipse IDE developers.
%
\subsubsection{Research about FLOSS}
Finally, there are papers that focus on FLOSS in itself.
%
% Using TransFlow to Analyze Open Source Developers' Evolution
\textbf{Costa \textit{et al.}}~\cite{costa2009:sbes} described Transflow, a tool aimed
at analyzing data about the co-evolution of the source code and the
developers' participation and social interaction in FLOSS projects.
%
%One Step More to Understand the Bug Report Duplication Problem
\textbf{Cavalcanti \textit{et al.}}~\cite{cavalcanti2010:sbes} discussed about the
bug report duplication problem that can have a negative influence on software
maintenance, in particular, because it increases time spent on report
analysis and validation.
%
They conducted a study about that using eight FLOSS projects (Bugzilla, Eclipse
Epiphany, Evolution, Firefox, GCC, Thunderbird, and Tomcat).
%
% An Empirical Study on the Structural Complexity Introduced by Core and
% Peripheral Developers in Free Software Projects
\textbf{Terceiro \textit{et al.}}~\cite{terceiro2010:core-periphery} studied how the
participation of developers in FLOSS projects can be associated with the
structural complexity of the project source code.
%
% A Study of the Relationships between Source Code Metrics and
% Attractiveness in Free Software Projects
\textbf{Meirelles \textit{et al.}}~\cite{Meirelles:sbes2010} analyzed source code
attributes as predictors for the attractiveness of FLOSS projects, i.e., its
ability to attract and retain users and developers, which is crucial for the
success of the project.

%------------------------------------------------------------------------------

\subsection{Analysis of tools papers mentioning FLOSS}

\begin{table*}[ht]
\begin{center}
\caption{Analysis of tools papers mentioning FLOSS}
\label{table:tools-papers}
\begin{tabular}{lrrrrrrrr|r}
  \hline
    Context                         & 2002 & 2004 & 2005  & 2006  & 2007  & 2008  & 2009  & 2010  & Total \\
  \hline
    Reference                       & 1    & 0    & 0     & 0     & 1     & 0     & 0     & 0     & 2 \\
    Promise to be FLOSS             & 0    & 0    & 1     & 1     & 0     & 1     & 0     & 1     & 4 \\
    use FLOSS                       & 1    & 1    & 0     & 0     & 0     & 0     & 1     & 1     & 4 \\
    FLOSS -- no license
    and/or no repository            & 0    & 1    & 0     & 5     & 1     & 2     & 2     & 3     & 14 \\
    FLOSS                           & 1    & 0    & 1     & 1     & 0     & 0     & 1     & 2     & 6 \\
  \hline
    Total                           & 3    & 2    & 2     & 7     & 2     & 3     & 4     & 7     & 30 \\
  \hline
\end{tabular}
\vspace{-1em}
\end{center}
\end{table*}

We analyzed 30 tools papers in which FLOSS terms are mentioned explicitly
to identify if they described actual FLOSS tools.
%
We separated them in four different groups according to how these tools papers
mentioned FLOSS and whether they explicitly mentioned a distribution
license and provided the location of a source code repository.
%
Table~\ref{table:tools-papers} shows the distribution of these
tools papers according to our classification.

%% Reference
%
Two tools are not FLOSS, but their respective papers cited FLOSS in their
bibliography:
% DDE – Draco Domain Editor
\textbf{DDE} by Garcia~\textit{et.al}~\cite{garcia2002:sbes-tools} and
%X-CORE: Um Serviço de Repositório Compartilhado e
%Distribuído de Componentes de Software
\textbf{X-CORE} by Oliveira~\textit{et.al}~\cite{oliveira2007:sbes-tools}.
%
%% Promise
Other four tools are not FLOSS but their authors promised that they would
be FLOSS in the future:
% ReqODE: Uma Ferramenta de Apoio à Engenharia de
% Requisitos Integrada ao Ambiente ODE
\textbf{ControlPro} by Moro~\textit{et.al}~\cite{moro2005:sbes-tools},
%
\textbf{ReqODE} by Martins~\textit{et.al}~\cite{martins2006:sbes-tools},
%
% CRISTA - Code Reading Implemented with Stepwise Abstraction
\textbf{CRISTA} by Porto~\textit{et.al}~\cite{porto2008:sbes-tools}, and
%
% StArt – Uma Ferramenta Computacional de Apoio à
% Revisão Sistemática
\textbf{StArt} by Zamboni~\textit{et.al}~\cite{zamboni2010:cbbsoft-tools}.
%
%% Uses/Dependent
%
Another group of four tools are not FLOSS but mentioned FLOSS in their
papers because they depend on FLOSS products:
%
% mudelgen: A Tool for Processing Mutant Operator Descriptions
\textbf{Mudelgen}, by Simão \textit{et.al}~\cite{simao2002:sbes-tools}, depends on Bison and
Flex, which are FLOSS compiler development tools.
%
%GAW: uma Ferramenta de Percepção de Grupo Aplicada no Desenvolvimento de Software
\textbf{GAW}, by Mangan \textit{et.al}~\cite{mangan2004:sbes-tools}, is a
Plug-in for Eclipse IDE.
%
% BAST
\textbf{BAST}, by Cavalcanti \textit{et.al}~\cite{cavalcanti2009:sbes-tools},
uses a MySQL dabase.
%
%ModelT2: Apoio Ferramental à Geração de Casos de Testes
%Funcionais a partir de Casos de Uso
\textbf{ModelT2}, by Albuquerque \textit{et.al}~\cite{albuquerque2010:cbbsoft-tools},
is related to the FLOSS UML tool BOUML.

\def\noLicenseFN{\footnote{Copyright law in most countries assumes that,
unless the author explicitly states otherwise, every work of creation such as
software is distributed under ``All Rights Reserved'' terms, so that third
parties are prevented from making derived works or even redistributing the work. That is
why, whenever a software product does not specify a distribution license, we
must assume it is not FLOSS.}}

%% FLOSS/"Freeware"
The majority of the papers mentioning FLOSS present their respective
tool as FLOSS, but 14 of them did not specify a license or repository.
Additionally, their respective websites were either unavailable or did
not contain this information. Although the authors had the intention to
make their tools available as FLOSS, they did not succeed in doing so.
Even in the best cases when the tool is actually available, they cannot
be considered FLOSS because they either do not made the source code
available or did not specify a license\noLicenseFN~ that complies with
either the Free Software defition or the Open Source definition.
%
This way
%C&L: Um Ambiente para Edição e Visualização de Cenários e Léxicos
\textbf{C\&L} by Felic\'{i}ssimo~\textit{et.al}~\cite{felicissimo2004:sbes-tools},
%
%Uma Ferramenta para Recuperação de Modelos de Processo de Software Reutilizáveis
\textbf{SearchEngine} by Sales~\textit{et.al}~\cite{sales2006:sbes-tools},
%
%Merlin: Interfaces CRUD Em Tempo de Execução
\textbf{Merlin} by Mrack~\textit{et.al}~\cite{mrack2006:-sbes-tools},
%
%Gerência Flexível de Processos de Software com o Ambiente WebAPSEE1
\textbf{WebAPSEE} by Lima~\textit{et.al}~\cite{lima2006:sbes-tools},
%
% Definição e Implementação de um Modelo de
% Rastreamento para Engenharia de Requisitos Distribuída
\textbf{Codipse-Req} by Brito and Vasconcelos~\cite{brito2006:sbes-tools},
%
% Captor: Um Gerador de Aplicações Configurável
\textbf{Captor} by Shimabukuro Jr.~\textit{et.al}~\cite{shimabukuroJr2006:sbes-tools},
%
% A Ferramenta BSmart para o Desenvolvimento Rigoroso de
% Aplicações Java Card com o Método Formal B∗
\textbf{BSmart} by Gomes~\textit{et.al}~\cite{gomes2007:sbes-tools},
%
% A Ferramenta Batcave para a Verificacao de Especificacoes Formais na Notacao B
\textbf{Batcave} by Marinho~\textit{et.al}~\cite{marinho2007:sbes-tools},
%
% TeTooDS - Testing Tool for Dynamic Systems
\textbf{TeTooDS} by Araujo and Delamaro~\textit{et.al}~\cite{araujo2008:sbes-tools},
%
% Desenvolvimento Evolutivo de Interfaces com o Usuário em uma Abordagem
% Baseada em Modelos e Múltipla Prototipagem: FastInterface
\textbf{FastInterface} by Oliveira and Lula Jr.~\textit{et.al}~\cite{oliveira2009:sbes-tools},
%
% RBTTool – Uma Ferramenta de Apoio à Abordagem de
% Teste de Software baseado em Riscos
\textbf{RBTTool} by Ven\^{a}ncio~\textit{et.al}~\cite{venancio2009:sbes-tools},
%
% Ferramenta de Apoio à Engenharia de Requisitos
% Integrada a um Ambiente Colaborativo de Código Aberto
\textbf{Fermine} by Almeida~\textit{et.al}~\cite{Almeida2010:cbbsoft-tools},
%
% AssistME – uma Ferramenta para Auxiliar a Refatoração
% para Aspectos de Tratamento de Exceções
\textbf{AssistME} by Queiroz~\textit{et.al}~\cite{queiroz2010:cbsoft-tools},
%
% ComSCId & DMAsp: Identificação de Interesses Transversais e Recuperação de
% Modelos de Classes Anotados a partir Aplicações OO Java
\textbf{ComSCId \& DMAsp} by Parreira
Jr.~\textit{et.al}~\cite{parreiraJr2010:cbsoft-tools}, cannot be
actually be considered as FLOSS tools.

%% FLOSS
Finally, only six tool papers provided all the information needed to
verify that they described actual FLOSS tools.
%
% Uma Visao Geral do Bugzilla, uma Ferramenta de Acompanhamento de Alteracoes
Reis~\cite{reis2002:sbes-tools} described in details the concepts and features of
\textbf{Bugzilla}, a well-known FLOSS issue tracking tool.
%
% GridUnit: Using the Computational Grid to Speed up Software Testing
Duarte \textit{et.al}~\cite{duarte2005:sbes-tools} described
\textbf{GridUnit}, a tool to execute software automated tests in grid
environments. It is available under the LGPL 2.1 license and its source
code is available in a CVS repository on SourceForge.
%
%% MVCASE - Incluindo Design Rationale para Auxilio a
% Modelagem em Projetos de Pesquisa
Paiva \textit{et.al}~\cite{paiva2006:sbes-tools} developed \textbf{MVCASE} to
support their model for design rationale capture and implementation. It is
available under the BSD license and its source code can be accessed from an
Subversion repository.
%
% Crab: Uma Ferramenta de Configuração e Interpretação de
% Métricas de Software para Avaliação de Qualidade de Código
Meirelles \textit{et.al}~\cite{meirelles2009:sbes-tools} presented
\textbf{Crab},
the first version of a tool for configuration and interpretation of source
code metrics. It was released under the BSD license in a Subversion repository.
%
It was later rewritten, renamed to Kalibro, relicensed under the LGPL
license, and made available from from a Git repository.
%
% Analizo: an Extensible Multi-Language Source Code Analysis
% and Visualization Toolkit
Terceiro \textit{et.al}~\cite{terceiro2010:analizo} described
\textbf{Analizo}, a free, multi-language, extensible source code analysis and
visualization toolkit that calculates a fair number of source code metrics,
generates dependency graphs, and makes software evolution analysis.
%
Analizo source code can be accessed from a Git repository and was released
under the GPL version 3 license.
%
%TaRGeT: a Model Based Product Line Testing Tool
Ferreira \textit{et.al}~\cite{ferreira2010:cbsoft-tools} created
\textbf{TaRGeT} to reduce the testing costs via a systematic approach that
generates test suites from use case specifications. Its source code can be
accessed from a Subversion repository and is licensed under the MIT license.

% vim: tw=80
\subsection{Analysis of the Software Engineering Education Forum papers}

The Software Engineering Education Forum had no published paper since
2008 and 2010 that addresses the use of FLOSS in SEE. We had a paper
accepted for the 2011 edition \cite{chavez2011}, in which we discuss
preliminar results of our experience with the use of FLOSS in SEE.

% vim: ts=2 sw=2 expandtab
